author = 'floordiv'
memory = {}                              # the main variable that contains other variables
# this keeps other variables as: {'program': {'variable': 'value', 'second_variable': 'value2'}, 'program2': {...}}


def write(name, key, value):
    if name in memory:                   # if segment with such name exists
        pass
    else:
        memory[name] = {}                # But if not, we create it
    memory[name][key] = value


def read(name, key):
    if name in memory:                   # if segment with such name exists
        if key in memory[name]:          # if variable with such name exists
            return memory[name][key]
        else:
            return None
    else:
        return None


def remove(name, key):
    if name in memory:                   # if segment with such name exists
        if key in memory[name]:          # if variable with such name exists
            del memory[name][key]
            return True
        else:
            return None
    else:
        return None


